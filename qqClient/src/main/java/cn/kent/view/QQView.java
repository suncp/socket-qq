package cn.kent.view;

import cn.kent.service.FileClientService;
import cn.kent.service.MessageClientService;
import cn.kent.service.UserClientService;
import cn.kent.util.Utility;

/**
 * 客户端的登录界面
 */
public class QQView {

    private boolean loop = true; // 控制是否显示菜单
    private String key = ""; // 获取用户输入
    private UserClientService userService = new UserClientService();
    private MessageClientService messageService = new MessageClientService();
    private FileClientService fileService = new FileClientService();

    public static void main(String[] args) {
        QQView qqView = new QQView();
        qqView.mainMenu();
    }

    // 显示主菜单
    private void mainMenu() {
        while (loop) {
            System.out.println("=========欢迎登陆网络及时通讯系统=========");
            System.out.println("\t\t 1 登录系统");
            System.out.println("\t\t 9 退出系统");
            System.out.print("请输入你的选择: ");

            key = Utility.readString(1);

            // 根据用户输入，来处理不同的逻辑
            switch (key) {
                case "1":
                    System.out.print("请输入用户号： ");
                    String userId = Utility.readString(50);
                    System.out.print("请输入密码： ");
                    String passwd = Utility.readString(50);
                    // 判断是否登录成功
                    if (userService.checkUser(userId, passwd)) {
                        System.out.println("欢迎（用户：" + userId + "）登录系统");
                        // 进入到二级菜单
                        while (loop) {
                            System.out.println("\n=========网络通信系统二级菜单(用户 " + userId + " )=======");
                            System.out.println("\t\t 1 显示在线用户列表");
                            System.out.println("\t\t 2 群发消息");
                            System.out.println("\t\t 3 私聊消息");
                            System.out.println("\t\t 4 发送文件");
                            System.out.println("\t\t 9 退出系统");
                            System.out.print("请输入你的选择: ");
                            key = Utility.readString(1);
                            String getterId = null;
                            switch (key) {
                                case "1":
                                    // 这里加个方法去获取用户列表
                                    userService.onlineFriendList();
                                    // System.out.println("显示在线用户列表");
                                    break;
                                case "2":
                                    System.out.print("请输入要群发给所有在线用户的消息[最多100字符]: ");
                                    String toAllContent = Utility.readString(100);
                                    messageService.sendMessageToAllOnline(toAllContent, userId);
                                    break;
                                case "3":
                                    System.out.print("请输入想聊天的用户号[离线则发送离线留言]: ");
                                    getterId = Utility.readString(50);
                                    System.out.print("请输入想说的话[100个字符以内]: ");
                                    String content = Utility.readString(100);
                                    // 将消息发送给服务端
                                    messageService.sendMessageToOne(content, userId, getterId);
                                    break;
                                case "4":
                                    System.out.print("请输入你想要把文件发送给的用户(用户离线将发送离线文件):");
                                    getterId = Utility.readString(50);
                                    System.out.print("请输入你想要发送的文件(格式：d:\\" + "\\" + "xx.jpg)：");
                                    String src = Utility.readString(100);
                                    System.out.print("请输入把文件发送到对方的路径(格式：d:\\" + "\\" + "yy.jpg)");
                                    String dest = Utility.readString(100);
                                    fileService.sendFileToOne(src, dest, userId, getterId);
                                    break;
                                case "9":
                                    // 调用方法，发送一个给服务器退出的message
                                    userService.logout();
                                    loop = false;
                                    break;
                            }
                        }
                    } else { // 登录服务器失败
                        System.out.println("==========登录失败===========");
                    }
                    break;
                case "9":
                    loop = false;
                    System.out.println("客户端退出系统...");
                    break;
            }
        }
    }
}
