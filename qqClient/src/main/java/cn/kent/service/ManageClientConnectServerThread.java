package cn.kent.service;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 管理客户端连接到服务端的线程类
 */
public class ManageClientConnectServerThread extends Thread {

    // 把多个线程放入一个HashMap集合中，key就是用户id
    private static ConcurrentHashMap<String, ClientConnectServerThread> map = new ConcurrentHashMap<>();

    // 将某个线程加入到集合
    public static void addClientConnectServerThread(String userId, ClientConnectServerThread thread) {
        map.put(userId, thread);
    }

    // 通过userId获取线程
    public static ClientConnectServerThread getClientConnectServerThread(String userId){
        return map.get(userId);
    }
}
