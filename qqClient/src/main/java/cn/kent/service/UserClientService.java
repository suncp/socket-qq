package cn.kent.service;

import cn.kent.common.User;
import cn.kent.common.Message;
import cn.kent.common.MessageType;
import cn.kent.util.DateUtil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * 客户端的一些方法
 * 用户注册和用户登录
 * 向服务器请求在线用户列表
 */
public class UserClientService {

    private User u = new User(); // 因为可能在其他地方使用user信息
    private Socket socket;

    /**
     * 根据userId和passwd验证该用户是否合法
     */
    public boolean checkUser(String userId, String passwd) {
        boolean b = false;

        // 创建User对象
        u.setUserId(userId);
        u.setPasswd(passwd);

        try {
            // 连接到服务器，发送u对象
            socket = new Socket(InetAddress.getByName("127.0.0.1"), 9999);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(u);

            // 读取从服务器回复的Message对象
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Message ms = (Message) ois.readObject();

            if (ms.getMsgType().equals(MessageType.MESSAGE_LOGIN_SUCCEE)) { // 登录成功
                // 创建一个和服务器保持通信的线程 -> 创建一个类 ClientConnectServerThread
                ClientConnectServerThread clientConnectServerThread = new ClientConnectServerThread(socket);
                clientConnectServerThread.start();
                ManageClientConnectServerThread.addClientConnectServerThread(userId, clientConnectServerThread);
                b = true;
            } else {
                // 登录失败，关闭socket
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b;
    }

    /**
     * 获取在线用户列表
     */
    public void onlineFriendList() {
        // 发送一个Message，类型
        Message message = new Message();
        message.setMsgType(MessageType.MESSAGE_GET_ONLINE_FRIEND);
        message.setSender(u.getUserId());
        message.setGetter("Server");
        message.setSendTime(DateUtil.getNowDateStr());

        // 发送服务器
        // 获取当前线程的Socket对应的oos
        try {
            ObjectOutputStream oos = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(u.getUserId()).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 给服务器发送一个退出系统的message
     */
    public void logout() {
        Message msg = new Message();
        msg.setMsgType(MessageType.MESSAGE_CLIENT_EXIT);
        msg.setSender(u.getUserId());// 指定退出的客户端id
        msg.setGetter("Server");
        msg.setSendTime(DateUtil.getNowDateStr());
        msg.setContent("退出系统");

        try {
            // ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            ObjectOutputStream oos = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(u.getUserId()).getSocket().getOutputStream());

            oos.writeObject(msg);
            System.out.println(u.getUserId() + " 退出系统");
            System.exit(0); // 结束进程
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
