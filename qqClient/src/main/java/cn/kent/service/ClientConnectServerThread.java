package cn.kent.service;

import cn.kent.common.Message;
import cn.kent.common.MessageType;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
// import java.util.Scanner;

/**
 * 后台单线程一直跑，去获取消息，没有消息时会阻塞
 */
public class ClientConnectServerThread extends Thread {
    // 该线程需要持有socket
    private Socket socket;
    // private Scanner scanner;

    public ClientConnectServerThread(Socket socket) {
        this.socket = socket;
        // this.scanner = new Scanner(System.in);
    }

    // 获取socket
    public Socket getSocket() {
        return this.socket;
    }

    @Override
    public void run() {
        // 因为Thread需要在后台和服务器通信，因此使用while
        while (true) {
            try {
                System.out.println("客户端线程，等待读取从服务端发送的消息");
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Message message = (Message) ois.readObject(); // 如果服务器没有发送Message对象，线程会阻塞


                // 如果读取到的是服务端返回的在线用户列表
                if (message.getMsgType().equals(MessageType.MESSAGE_RET_ONLINE_FRIEND)) {
                    // 取出在线用户列表，并显示
                    // 此处做个返回形式的规定为(空格分割)： 100 200 太上老君
                    String content = message.getContent();
                    String[] onlineUsers = content.split(" ");
                    System.out.println("\n在线用户列表展示如下：");
                    for (int i = 0; i < onlineUsers.length; i++) {
                        System.out.println("在线用户：" + onlineUsers[i]);
                    }
                } else if (message.getMsgType().equals(MessageType.MESSAGE_COMM_MES)) {
                    // 把从服务器转发的消息[私聊]，显示到控制台
                    if (message.isOffLineBool()) { // 判断是否离线消息
                        System.out.println("\n离线留言:" + message.getSender()
                                + " 对 " + message.getGetter() + " 说： " + message.getContent());
                    } else {
                        System.out.println("\n" + message.getSender()
                                + " 对 " + message.getGetter() + " 说： " + message.getContent());
                    }
                } else if (message.getMsgType().equals(MessageType.MESSAGE_TO_ALL_MES)) {
                    // 把从服务器转发的消息[群发]，显示到控制台
                    System.out.println("\n" + message.getSender()
                            + "通过群发对 大家 说： " + message.getContent());
                } else if (message.getMsgType().equals(MessageType.MESSAGE_FILE_MES)) {
                    // 判断是否是离线消息
                    if (message.isOffLineBool()) {
                        System.out.println("\n离线消息：\t" + message.getSender() + " 给 " + message.getGetter()
                                + " 发文件： " + message.getSrc() + " 到我的电脑目录： " + message.getDest());

                            FileOutputStream fos = new FileOutputStream(message.getDest());
                            fos.write(message.getFileBytes());
                            fos.close();
                            System.out.println("保存文件成功~");

                        // todo: main线程仍然会捕获到输入信息，等我研究下多线程再回来搞，先写成指定路径保存
                        // while (true) {
                        //     System.out.print("是否想要保存到指定目录？(Y/N):\n");
                        //     String bool = readKeyBoard(1, true);
                        //     String dest;
                        //     if ("Y".equals(bool)) {
                        //         dest = message.getDest();
                        //     } else if ("N".equals(bool)) {
                        //         System.out.print("请输入你想要保存的目录(d:\\" + "\\" + "mm.jpg)：\t");
                        //         dest = readKeyBoard(50, true);
                        //         // todo: 校验路径是否合法
                        //     } else {
                        //         System.out.println("输入不合法，请重新输入(Y/N)！");
                        //         continue;
                        //     }
                        //     FileOutputStream fos = new FileOutputStream(dest);
                        //     fos.write(message.getFileBytes());
                        //     fos.close();
                        //     System.out.println("保存文件成功~");
                        //     break;
                        // }
                    } else {
                        System.out.println("\n" + message.getSender() + " 给 " + message.getGetter()
                                + " 发文件： " + message.getSrc() + " 到我的电脑目录： " + message.getDest());

                        // 将文件读取到相应目录
                        FileOutputStream fos = new FileOutputStream(message.getDest());
                        fos.write(message.getFileBytes());
                        fos.close();
                        System.out.println("保存文件成功~");
                    }
                } else {
                    System.out.println("得到其他类型的message，暂时不处理...");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //
    // private String readKeyBoard(int limit, boolean blankReturn) {
    //
    //     //定义了字符串
    //     String line = "";
    //
    //     //scanner.hasNextLine() 判断有没有下一行
    //     while (scanner.hasNextLine()) {
    //         line = scanner.nextLine();//读取这一行
    //
    //         //如果line.length=0, 即用户没有输入任何内容，直接回车
    //         if (line.length() == 0) {
    //             if (blankReturn) return line;//如果blankReturn=true,可以返回空串
    //             else continue; //如果blankReturn=false,不接受空串，必须输入内容
    //         }
    //
    //         //如果用户输入的内容大于了 limit，就提示重写输入
    //         //如果用户输入的内容 >0 <= limit ,我就接受
    //         if (line.length() < 1 || line.length() > limit) {
    //             System.out.print("输入长度（不能大于" + limit + "）错误，请重新输入：");
    //             continue;
    //         }
    //         break;
    //     }
    //
    //     return line;
    // }
}
