package cn.kent.service;

import cn.kent.common.Message;
import cn.kent.common.MessageType;
import cn.kent.util.DateUtil;

import java.io.IOException;
import java.io.ObjectOutputStream;

public class MessageClientService {

    public void sendMessageToOne(String content, String sendId, String getterId) {
        // 构建message
        Message message = new Message();
        message.setMsgType(MessageType.MESSAGE_COMM_MES); // 普通的聊天消息
        message.setContent(content);
        message.setSender(sendId);
        message.setGetter(getterId);
        message.setSendTime(DateUtil.getNowDateStr());

        // 发送服务端
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    ManageClientConnectServerThread.getClientConnectServerThread(sendId).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void sendMessageToAllOnline(String content, String sendId) {
        // 构建message
        Message message = new Message();
        message.setMsgType(MessageType.MESSAGE_TO_ALL_MES); // 群发消息类型
        message.setContent(content);
        message.setSender(sendId);
        message.setSendTime(DateUtil.getNowDateStr());
        System.out.println(sendId + " 对大家说: " + content);

        // 发送服务端
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    ManageClientConnectServerThread.getClientConnectServerThread(sendId).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
