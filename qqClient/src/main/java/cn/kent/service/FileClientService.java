package cn.kent.service;

import cn.kent.common.Message;
import cn.kent.common.MessageType;
import cn.kent.util.DateUtil;

import java.io.*;

/**
 * 文件传输相关service
 */
public class FileClientService {

    /**
     * @param src      源文件
     * @param dest     把该文件传输到哪个目录
     * @param senderId 发送方用户id
     * @param getterId 接收方用户id
     */
    public void sendFileToOne(String src, String dest, String senderId, String getterId) {
        Message message = new Message();
        message.setMsgType(MessageType.MESSAGE_FILE_MES);
        message.setSendTime(DateUtil.getNowDateStr());
        message.setGetter(getterId);
        message.setSender(senderId);
        message.setSrc(src);
        message.setDest(dest);

        // 需要将文件读取
        FileInputStream fis = null;
        byte[] fileBytes = new byte[(int) new File(src).length()];

        try (FileInputStream fileInputStream = new FileInputStream(src)) {
            fileInputStream.read(fileBytes);
            // 将字节数组设置到对应的msg中
            message.setFileBytes(fileBytes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // 提示信息
        System.out.println("\n" + senderId + " 给 " + getterId + " 发送文件： " + src
                + " 到对方的目录 " + dest);

        // 发送
        try {
            ObjectOutputStream oos =
                    new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            oos.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
