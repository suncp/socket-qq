package cn.kent.service;

import java.util.HashMap;
import java.util.Iterator;

public class ManageClientThreads {
    private static HashMap<String, ServerConnectClientThread> map = new HashMap<>();

    public static void removeServerConnectClientThread(String userId) {
        map.remove(userId);
    }

    public static void addClientThread(String userId, ServerConnectClientThread thread) {
        map.put(userId, thread);
    }

    public static ServerConnectClientThread getClientThread(String userId) {
        return map.get(userId);
    }

    public static HashMap<String, ServerConnectClientThread> getMap() {
        return map;
    }

    // 编写方法，返回在线用户列表
    public static String getOnlineUsers(){
        // 集合遍历key即可
        Iterator<String> iterator = map.keySet().iterator();
        String onlineUsers = "";
        while (iterator.hasNext()) {
            onlineUsers += iterator.next().toString() + " ";
        }
        return onlineUsers;
    }

}
