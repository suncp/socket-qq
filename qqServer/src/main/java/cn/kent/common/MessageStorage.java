package cn.kent.common;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class MessageStorage {
    /**
     * 接收方userid作为key，表示每个用户可以给多个用户发送离线消息
     * 用于存储离线留言消息
     */
    private static ConcurrentHashMap<String, List<Message>> offMessageStorageMap = new ConcurrentHashMap<>();

    public static ConcurrentHashMap<String, List<Message>> getOffMessageStorageMap() {
        return offMessageStorageMap;
    }

    public static List<Message> getMessageStorage(String userId) {
        return offMessageStorageMap.get(userId);
    }

    public static void addMessageStorage(Message message) {
        List<Message> list1 = offMessageStorageMap.get(message.getGetter());
        if (list1 == null || list1.size() <= 0) {
            list1 = new CopyOnWriteArrayList<Message>();
        }
        list1.add(message);
        offMessageStorageMap.put(message.getGetter(), list1);
    }
}
