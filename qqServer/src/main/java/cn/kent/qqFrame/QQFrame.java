package cn.kent.qqFrame;

import cn.kent.service.QQServer;

/**
 * 该类创建一个QQServer对象，相当于启动了后台了服务
 */
public class QQFrame {
    public static void main(String[] args) {
        new QQServer();
    }
}
